#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_manaus.mk

COMMON_LUNCH_CHOICES := \
    lineage_manaus-user \
    lineage_manaus-userdebug \
    lineage_manaus-eng
